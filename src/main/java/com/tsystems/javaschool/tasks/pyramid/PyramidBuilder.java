package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        Integer numOfRows = rowsEnough(inputNumbers.size());
        if (numOfRows == null || inputNumbers.contains(null))
            throw new CannotBuildPyramidException();
        int[][] pyramid = buildPyramid(inputNumbers, numOfRows);
        return pyramid;
    }

    private int[][] buildPyramid(List<Integer> numbers, int numOfRows) {
        int[][] result = new int[numOfRows][numOfRows*2-1];
        Collections.sort(numbers);
        Collections.reverse(numbers);
        int skipCounter = 0, numberCounter = 0;
        boolean flag;
        for (int i = (result.length-1); i >= 0; i--) {
            flag = true;
            for (int j = (result[0].length-1)-skipCounter; j >= skipCounter; j--) {
                if (flag) {
                    result[i][j] = numbers.get(numberCounter);
                    numberCounter++;
                }
                flag = !flag;
            }
            skipCounter++;
        }
        return result;
    }

    private Integer rowsEnough(int size) {
        int row = 0;
        int counter = 0;
        boolean flag = false;
        for (int i = 0; i <= size; i++) {
            flag = false;
            if (counter == row) {
                row++;
                counter = 0;
                flag = true;
            }
            counter++;
        }
        if (flag)
            return --row;
        return null;
    }
}
