package com.tsystems.javaschool.tasks.calculator;

public class Operator implements Comparable<Operator> {
    private char operationChar;

    private int precedence;

    public Operator(char op) {
        switch (op) {
            case '*':
            case '/':
                precedence = 2;
                break;
            case '-':
            case '+':
                precedence = 1;
                break;
            default:
                precedence = 0;
        }
        this.operationChar = op;
    }

    public char getOperationChar() {
        return operationChar;
    }

    @Override
    public int compareTo(Operator o) {
        return this.precedence - o.precedence;
    }

    @Override
    public String toString() {
        return Character.toString(operationChar);
    }
}