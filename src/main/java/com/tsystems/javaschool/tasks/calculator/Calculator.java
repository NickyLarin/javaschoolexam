package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public static final String OPERATORS = "+-*/";

    private static boolean isOperator(char character) {
        return OPERATORS.contains(Character.toString(character));
    }

    public String evaluate(String statement) {
        // TODO: Implement the logic here
        Queue<String> RPNOutput;
        try {
            RPNOutput = infixToRPN(statement);
        } catch (CalculatorException e) {
            RPNOutput = null;
        }
        return evaluateRPN(RPNOutput);
    }

    private String evaluateRPN(Queue<String> rpn) {
        if (rpn == null)
            return null;

        Deque<Double> stack = new LinkedList<>();
        while (!rpn.isEmpty()) {
            String s = rpn.remove();
            if (!isOperator(s.charAt(0))) {
                try {
                    stack.push(Double.parseDouble(s));
                } catch (NumberFormatException e) {
                    return null;
                }
            } else {
                double secondOperand;
                double firstOperand;
                try {
                    secondOperand = stack.pop();
                    firstOperand = stack.pop();
                } catch (NoSuchElementException e) {
                    return null;
                }
                char op = s.charAt(0);
                stack.push(performOperation(op, firstOperand, secondOperand));
            }
        }
        Double result = stack.pop();
        if (Double.isInfinite(result))
            return null;
        if (result == Math.floor(result))
            return Integer.toString(result.intValue());
        return result.toString();
    }

    private Double performOperation(char op, double f, double s) {
        Double result = null;
        switch (op) {
            case '+':
                result = f + s;
                break;
            case '-':
                result = f - s;
                break;
            case '*':
                result = f * s;
                break;
            case '/':
                result = f / s;
                break;
        }
        return result;
    }

    private Queue<String> infixToRPN(String statement) throws CalculatorException {
        if (statement == null || statement.length() == 0)
            return null;

        Queue<String> output = new LinkedList<>();
        Deque<Operator> operatorStack = new LinkedList<>();

        StringBuilder currentString = new StringBuilder();
        for (char c: statement.toCharArray()) {
            if (Character.isDigit(c) || c == '.') {
                currentString.append(c);
                continue;
            } else if (!Character.isDigit(c) && currentString.length() > 0) {
                output.add(currentString.toString());
                currentString.setLength(0);
            }

            if (c == '(') {
                operatorStack.push(new Operator(c));
                continue;
            }
            if (c == ')') {
                while (!operatorStack.isEmpty()) {
                    Operator op = operatorStack.pop();
                    if (op.getOperationChar() == '(')
                        break;
                    output.add(op.toString());
                }
                continue;
            }

            if (isOperator(c)) {
                Operator op = new Operator(c);
                while (!operatorStack.isEmpty() && operatorStack.peek().compareTo(op) >= 0) {
                    output.add(operatorStack.pop().toString());
                }
                operatorStack.push(new Operator(c));
                continue;
            }
            throw new CalculatorException("Statement to RPN parsing failed");
        }
        if (currentString.length() > 0) {
            output.add(currentString.toString());
        }
        while (!operatorStack.isEmpty()) {
            output.add(operatorStack.pop().toString());
        }
        return output;
    }
}