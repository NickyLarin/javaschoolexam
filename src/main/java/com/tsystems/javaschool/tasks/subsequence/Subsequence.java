package com.tsystems.javaschool.tasks.subsequence;

import java.util.LinkedList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if (x == null || y == null)
            throw new IllegalArgumentException();
        if (x.size() > y.size())
            return false;
        return checkByRemoving(x, y, y.size()-x.size());
    }

    private boolean checkByRemoving(List x, List y, int elementsCanRemoved) {
        List temp = new LinkedList(y);
        boolean found = x.equals(y);
        for (int i = 0; !found && i < y.size(); i++) {
            Object element = temp.remove(i);
            if (elementsCanRemoved-1 > 0) {
                found = checkByRemoving(x, temp, elementsCanRemoved-1);
            } else {
                if (x.equals(temp))
                    found = true;
            }
            temp.add(i, element);
        }
        return found;
    }
}
